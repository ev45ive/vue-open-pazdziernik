# Git clone
pwd
cd ..
git clone https://bitbucket.org/ev45ive/vue-open-pazdziernik.git vue-open-pazdziernik
cd vue-open-pazdziernik/vue-spa
npm i 
npm run serve

# Pobranie zmian
git stash -u
git pull -u -f origin master

# INstalacje

# Visual Studio
https://marketplace.visualstudio.com/items?itemName=octref.vetur
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

# Chrome
https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

# Mac NPM no sudo
npm config set prefix ~/.npm
r ~/.zshrc, ~/.profile, or equivalent you should also set something like export PATH="$HOME/.npm/bin:$PATH"

# Mac node update
brew uninstall node
brew update
brew upgrade
brew cleanup
brew install node
sudo chown -R $(whoami) /usr/local
brew link --overwrite node
brew postinstall node

# Server prosty http
sudo npm i -g http-server
npm i -g http-server
cd .
http-server
http://192.0.0.1:8080/
<!--  -->
npx http-server

# Vue 
npm i -g @vue/cli
vue create vue-spa
<!-- lub -->
vue create vue-spa

Vue CLI v4.5.7
? Please pick a preset:
> Manually select features
? Check the features needed for your project: 
 (*) Choose Vue version
 (*) Babel
 (*) TypeScript
 (*) Progressive Web App (PWA) Support        
 (*) Router
 (*) Vuex
 ( ) CSS Pre-processors
 (*) Linter / Formatter
 (*) Unit Testing
>( ) E2E Testing

Vue CLI v4.5.7
? Please pick a preset: Manually select features
? Check the features needed for your project: Choose Vue version, Babel, TS, PWA, Router, Vuex, CSS Pre-processors, Linter, Unit, E2E
? Choose a version of Vue.js that you want to start the project with 2.x
? Use class-style component syntax? Yes
? Use Babel alongside TypeScript (required for modern mode, auto-detected polyfills, transpiling JSX)? Yes
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features:
? Pick a unit testing solution: Jest
<!-- ? Pick an E2E testing solution: WebdriverIO -->
<!-- ? Pick browsers to run end-to-end test on Chrome -->
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? No

# No TypeScript
vue create vue-no-typescript-placki

Vue CLI v4.5.7
? Please pick a preset: Manually select features
? Check the features needed for your project: Choose Vue version, Babel
? Choose a version of Vue.js that you want to start the project with 2.x      
? Where do you prefer placing config for Babel, ESLint, etc.? In package.json
? Save this as a preset for future projects? No

# Enviroment and params
$ npm run serve -- --port 8080
https://pl.wikipedia.org/wiki/Zmienna_%C5%9Brodowiskowa
// Windows
set PORT=8080
unset PORT
// Unix/MacOS
PORT=8080
unset PORT


# Ankieta
https://cutt.ly/lgsBhGO