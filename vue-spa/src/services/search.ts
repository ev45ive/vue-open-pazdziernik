import { Album, AlbumSearchResponse } from '@/model/Search';
import axios from 'axios'


export class MusicSearchService {

  constructor() { }

  searchAlbums(query: string): Promise<Album[]> {
    return axios
      .get<AlbumSearchResponse>("https://api.spotify.com/v1/search", {
        params: {
          type: "album",
          q: query
        },
      })
      .then(resp => resp.data.albums.items)
  }
}