export { MusicSearchService } from './search';
export { AuthService } from './auth'

import axios from 'axios';
import { AuthService } from './auth';
import { MusicSearchService } from './search';
import { UserService } from './users';


export const auth = new AuthService({
  auth_url: "https://accounts.spotify.com/authorize",
  client_id: "23d7b1227f9b4eb9a5dcb2ff840338c9",
  response_type: "token",
  redirect_uri: "http://localhost:8080/",
  show_dialog: false,
  scopes: [
    'playlist-read-collaborative',
    'playlist-modify-private',
    'playlist-modify-public',
    'playlist-read-private',
  ]
});

export const user = new UserService(auth);

export const search = new MusicSearchService();


axios.interceptors.request.use(config => {
  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  // config.withCredentials = true // include cookies

  config.headers['Authorization'] = `Bearer ${auth.getToken()}`
  return config
})

axios.interceptors.response.use(config => config, error => {

  // If we have error details in response from server
  if (error.response?.data) {
    // If not authorised
    if (error.response.status == 401) {
      auth.autologin && setTimeout(() => auth.authorize(), 1000); // autologin
    }
    return Promise.reject(new Error(error.response.data?.error?.message))
  } else {
    // All other unexpected errors - log, and display message
    console.error(error.message)
    return Promise.reject(new Error("Unexpected error"))
  }
})