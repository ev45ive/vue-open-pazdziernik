import axios from 'axios';
import { UserProfile } from '../model/UserProfile';
import { AuthService } from './auth';


export class UserService {

  userProfile: UserProfile | null = null

  constructor(private auth: AuthService) { }

  getUserProfile() {
    if (this.userProfile) {
      return Promise.resolve(this.userProfile)
    } else if (this.auth.getToken()) {
      return this.getCurrentUserProfile()
    } else {
      return Promise.reject(new Error('Not Logged In'))
    }
  }

  getCurrentUserProfile() {
    return axios.get<UserProfile>(`https://api.spotify.com/v1/me`).then(resp => resp.data).then(user => {
      this.userProfile = user;
      return user
    })
  }
}

