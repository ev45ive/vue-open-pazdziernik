import { Playlist } from '@/model/Playlist';
import { Album, AlbumSearchResponse, PagingObject } from '@/model/Search';
import axios, { AxiosResponse } from 'axios'

type PlaylistCreatePayload = {
  // [k: string]: string
  // name: Playlist['name']
  [k in 'name' | 'public' | 'description']: Playlist[k]
}


export class PlaylistsService {

  constructor() { }

  getCurrentUserPlaylists(): Promise<Playlist[]> {
    return axios.get<PagingObject<Playlist>>('https://api.spotify.com/v1/me/playlists')
      .then(resp => resp.data.items)
  }

  gePlaylistsById(playlist_id: Playlist['id']): Promise<Playlist> {
    return axios.get<Playlist>(`https://api.spotify.com/v1/playlists/${playlist_id}`)
      .then(resp => resp.data)
  }

  createPlaylistForUser(user_id: string, draft: PlaylistCreatePayload): Promise<Playlist> {
    const { name, description, public: isPublic } = draft

    return axios.post<PlaylistCreatePayload, AxiosResponse<Playlist>>(`https://api.spotify.com/v1/users/${user_id}/playlists`, {
      name, description, public: isPublic
    }).then(resp => resp.data)
  }

  updatePlaylist(playlist_id: string, draft: PlaylistCreatePayload) {
    const { name, description, public: isPublic } = draft

    return axios.put<PlaylistCreatePayload>(`https://api.spotify.com/v1/playlists/${playlist_id}`, {
      name, description, public: isPublic
    })
  }
}
