import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
// import About from "../views/About.vue";
// import Home from "../views/Home.vue";
// import Playlists from "../views/Playlists.vue";
// import MusicSearch from "../views/MusicSearch.vue";
// import PlaylistItems from "../views/PlaylistItems.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    // component: Home
    redirect: '/search'
  },
  {
    path: "/playlists/items",
    name: "PlaylistItems",
    component: () => import(/* webpackChunkName: "playlist-items" */ "../views/PlaylistItems.vue")
  },
  {
    path: "/playlists/:playlist_id?",
    name: "Playlists",
    component: () => import(/* webpackChunkName: "search" */ "../views/Playlists.vue"),
    // beforeEnter(to, from, next) { next() },
  },
  {
    path: "/search",
    name: "Search",
    component: () => import(/* webpackChunkName: "search" */ "../views/MusicSearch.vue")
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: '*',
    // component: PageNotFound
    component: {
      render: h => h('h1', {}, 'Page not found')
    }
  }
];

const router = new VueRouter({
  // mode: "hash",
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'active',
});

export default router;
