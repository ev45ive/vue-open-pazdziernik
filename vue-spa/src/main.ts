import Component from 'vue-class-component'

// import AlbumCard from "./components/albums/AlbumCard.vue";
// Vue.component('AlbumCard',AlbumCard)

// https://class-component.vuejs.org/guide/additional-hooks.html
// Register the router hooks with their names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate'
])

import Vue from "vue";
import "./registerServiceWorker";
import { AuthService } from './services';
import store from "./store";
import router from "./router";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.filter('yesno', (value: string, yes = "Yes", no = "no") => {
  return value ? yes : no;
});
import Card from './components/Card.vue'
Vue.component('Card', Card)
import Button from './components/Button.vue'
Vue.component('Button', Button)

const app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

; (window as any).app = app




// declare module "vue/types/vue" {
//   interface Vue {
//     auth: AuthService
//   }
// }

// declare module "vue/types/options" {
//   interface ComponentOptions<V extends Vue> {
//     auth?: AuthService
//   }
// }