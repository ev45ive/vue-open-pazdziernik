
interface Entity {
  id: string;
  name: string;
}

interface Track extends Entity {
  duration: number
}

export interface Playlist extends Entity {
  public: boolean;
  description: string;
  tracks?: Track[]
}

// const p:Playlist = {}

// if(p.tracks){
//   p.tracks.length
// }