// p = fetch('http://localhost:3000/mockdata.json')
// console.log(123)

function echo(msg,cb){
    setTimeout(()=>{
        //response
        cb(msg)
    },2000)
}

echo('Alice',(p)=>{
    p += ' ma kota'
    console.log('res: '+p)
})

console.log('juz')

// =====

// callback hell // pyramid of doom

function echo(msg,cb){
    setTimeout(()=>{
        //response
        cb(msg)
    },2000)
}

echo('Alice',(p)=>{
    echo(p + ' ma ',p => {
        echo(p + ' kota',p => {
            console.log('res: '+p)
        });
    });
})

console.log('juz')
// ===

// p = fetch('http://localhost:3000/mockdata.json')
// console.log(123)

function echo(msg){
  return new Promise((resolve)=>{
      setTimeout(()=>{
          resolve(msg)
      },2000)
  })
}

p = echo('Alice')

p.then(console.log)
p.then(console.log)

// echo('Alice',(p)=>{
//     echo(p + ' ma ',p => {
//         echo(p + ' kota',p => {
//             console.log('res: '+p)
//         });
//     });
// })

p2 = p.then((p) => p + ' ma kota')
p2.then(console.log)

p2 = p.then((p) => echo(p + ' ma asynchronicznego kota'))
p2.then(console.log)

console.log('juz')

/// === 

p = fetch('http://localhost:3000/mockdata.json')
// p.then(res => console.log(res.json()))
// Promise {<pending>}
// Promise {<pending>}

// p.then(res => res.json().then(console.log) )
p.then(res => res.json() )
.then(albums => console.log(albums))
// Promise {<pending>}
// VM79762:5 (4) [{…}, {…}, {…}, {…}]
// VM76522:1 Fetch finished loading: GET "http://localhost:3000/mockdata.json".

/// ====

// p = fetch('http://localhost:3000/mockdata.json')
// console.log(123)

function echo(msg,err){
  return new Promise((resolve, reject)=>{
      setTimeout(()=>{
      err? reject(err): resolve(msg)
      },2000)
  })
}

echo('Alice','upss')
// p.then(console.log, console.error)
.then( user => user + ' ma ', err => 'Nie ma usera')
.then( userma => echo(userma + ' kota'))
.catch(err => ' nie ma kota')
.then(console.log)


//// ===========

/// Sync
function getAlbums(){
    return []
}

albums = getAlbums()
// []

//// ===========
/// Async - callback

function getAlbums(callback){ 
    setTimeout(()=>{
        callback([1,2,3])
    },2000);
}
getAlbums(albums => console.log(albums))


//// ===========
/// Async - Promise 
function getAlbums(){ 
    
    return new Promise((resolve)=>{
       setTimeout(()=>{
             resolve([1,2,3])
        },2000);
    })
}

albumsPromise = getAlbums()
albumsPromise.then(albums => console.log(albums))